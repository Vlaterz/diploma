#include "neuralnet.h"

NeuralNet::NeuralNet()
{

}

NeuralNet::NeuralNet(std::vector<Neuron *> neurons, int Depth)
    :vecNeurons(neurons),depth(Depth)
{}
/*
std::vector<double> NeuralNet::Update(const std::vector<double>& inputs, const UPDATE_TYPE update_type)
{
    std::vector<double> outputs;

    int flush_count = 1;

    for(int i = 0; i < flush_count; ++i)
    {
        outputs.clear();
        int neuron_idx = 0;

        // The expected order of neurons: INPUT .. INPUT, BIAS, HIDDEN .. HIDDEN
        while(m_neurons[neuron_idx].Type == NeuronType::INPUT)
        {
            m_neurons[neuron_idx].OutputSignal = inputs[neuron_idx];
            ++neuron_idx;
        }

        // set the bias neuron output to 1
        m_neurons[neuron_idx++].OutputSignal = 1;

        // step through the network a neuron at a time
        while (neuron_idx < m_neurons.size())
        {
            //sum this neuron's inputs by iterating through all the links into
            //the neuron
            double sum = cpplinq::from(m_neurons[neuron_idx].InLinks)
                >> cpplinq::select([](const Link& link)
                    {
                        return link.Weight * link.In->OutputSignal;
                    })
                >> cpplinq::sum();

            //now put the sum through the activation function and assign the
            //value to this neuron's output
            m_neurons[neuron_idx].OutputSignal =
                sigmoid(sum, m_neurons[neuron_idx].ActivationResponse);

            if (m_neurons[neuron_idx].Type == NeuronType::OUTPUT)
            {
                //add to our outputs
                outputs.push_back(m_neurons[neuron_idx].OutputSignal);
            }

            //next neuron
            ++neuron_idx;
        }
    }

    if(update_type == UPDATE_TYPE::SNAPSHOT)
    {
        for(auto& neuron : m_neurons)
        {
            neuron.OutputSignal = 0.0;
        }
    }
    return std::move(outputs);
}
*/

std::vector<double> NeuralNet::Update(const std::vector<double> &inputs, const run_type type)
{
    //std::cout<<"NN::Update"<<std::endl;
    std::vector<double>outputs;
    int FlushCount=0;
    if(type==snapshot)
    {
        FlushCount=depth;
    }
    else
    {
        FlushCount=1;
    }
    for(int i = 0 ; i<FlushCount ; i++)
    {
        outputs.clear();
        unsigned int neuronIndex = 0;
        while(vecNeurons[neuronIndex]->NeuronType == input)
        {
            vecNeurons[neuronIndex]->output = inputs[neuronIndex];
            ++neuronIndex;
        }
        //set the output of the bias to 1
        //m_vecpNeurons[cNeuron++]->dOutput = 1;

        std::vector<Neuron*> hiddenNeurs;
        std::vector<Neuron*> outputNeurs;
        for(auto& neur : vecNeurons)
        {
            //neur->showInfo();
            if(neur->NeuronType == hidden)
            {
                hiddenNeurs.push_back(neur);
            }
            else if(neur->NeuronType == output)
            {
                outputNeurs.push_back(neur);
            }
        }

        for(auto& neur : hiddenNeurs)
        {
            double sum=0;
            for(auto& l : neur->linksIn)//every "In" link
            {
               double weight = l.weight;
               double neuronOutput = l.In->output; //output of neur the link is coming from
               sum+=weight*neuronOutput;
            }
           // std::cout<<"sum:"<<sum<<" act Rsponse:"<<neur->activationResponse<<std::endl;
            neur->output=Sigmoid(sum,neur->activationResponse);
           // neur->showInfo();
        }
        for(auto& neur : outputNeurs)
        {
            double sum=0;
            for(auto& l : neur->linksIn)//every "In" link
            {
               double weight = l.weight;
               double neuronOutput = l.In->output; //output of neur the link is coming from
               sum+=weight*neuronOutput;
            }

            neur->output=Sigmoid(sum,neur->activationResponse);
            outputs.push_back(neur->output);
        }

        if(type == snapshot)
        {
            for(unsigned int i=0 ; i<vecNeurons.size() ; i++)
            {
                vecNeurons[i]->output=0;
            }
        }
    }
    return outputs;
}
