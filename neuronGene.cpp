#include "neuronGene.h"
NeuronGene::NeuronGene()
{

}

NeuronGene::NeuronGene(int neuronID, neuron_type NT, bool rec, double X, double Y)
    :ID(neuronID),type(NT),recurrent(rec),x(X),y(Y)
{
    geneType = neuron;
}

void NeuronGene::showInfo()
{           
    std::string typeStr;
    if(type==input)
        typeStr="input";
    else if(type==output)
        typeStr="output";
    else if(type==hidden)
        typeStr="hidden";
    else if(type==bias)
        typeStr="bias";
    else if(type==unknown)
        typeStr="unknown";

    std::cout<<"ID:"<<ID
            <<" type:"<<typeStr
           <<" loop:"<<recurrent
          <<" coords:("<<x<<","<<y<<")"
         <<std::endl;
}


