#ifndef INNOVATION_H
#define INNOVATION_H
#include <vector>
#include <iostream>
#include "enums.hpp"

class Innovation
{
public:
    Innovation(){}
    Innovation(int OUTneuron,int INneuron ,int innovationID);
    Innovation(int idOfNeuron, neuron_type type, int innovationID);
    Innovation(int out, int in, innov_type InnovType, neuron_type neurType, int innovationID);
    innov_type innovationType;
    int neuronIN;
    int neuronOUT;
    int innovationID;
    int neuronID;
    neuron_type typeOfNeuron;

};

class Innovations
{
public:
    Innovations();
    std::vector<Innovation> innovations;
    unsigned int lastInnovationId;

    void addInnovation(int fromNeuron, int toNeuron);//new link
    void addInnovation(int neuronID, neuron_type type );//new neuron

    int getInnovation(int neuronFrom, int neuronTo);
    int getInnovation(int neuronID, neuron_type neuroType);
    int getLastInnovationID();
    int getNeuronId(int innovationID);
    int getLastNeuronId();
    int getLastSynapseID();
    Innovation* getInnovationByID(int innovID);
    void showInfo();

};

#endif // INNOVATION_H
