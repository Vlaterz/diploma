#include "ga.h"

void Ga::sortSpecies()
{
    for(unsigned int i = 0; i<species.size() ; i++)
    {
        for(unsigned int j = i; j<species.size() ; j++)
        {
            if(species[i].Leader()->Fitness() < species[j].Leader()->Fitness())
            //if(species[i].averageAdjFitness < species[j].averageAdjFitness)
            {
                Species temp = species[i];
                species[i]=species[j];
                species[j]=temp;
            }
        }
    }
}

Genome Ga::crossover(Genome &mom, Genome &dad, Innovations &I)
{
    mom.sortGenes();
    mom.sortNeurons();
    dad.sortGenes();
    dad.sortNeurons();
    //std::cout<<"ga::crossover::CrossOVER start:"<<std::endl;
    parent_type best;
    if(mom.Fitness() == dad.Fitness())
    {
        if(mom.numGenes()==dad.numGenes())
        {
            //std::cout<<"ga::crossover::same fitness, same number of genes random best"<<std::endl;
            best = (parent_type)RandINT(0,1);
        }
        else
        {
            if(mom.numGenes() < dad.numGenes())
            {
                //std::cout<<"ga::crossover::BEST IS MOM(by genes num)"<<std::endl;
                best=MOM;
            }
            else
            {
                //std::cout<<"ga::crossover::BEST IS DAD(by genes num)"<<std::endl;
                best=DAD;
            }
        }
    }
    else //moms fitness != dads fitness
    {
        if(mom.Fitness()>dad.Fitness())
        {
            //std::cout<<"ga::crossover::BEST IS MOM(by fitness)"<<std::endl;
            best = MOM;
        }
        else
        {
            //std::cout<<"ga::crossover::BEST IS DAD(by fitness)"<<std::endl;
            best = DAD;
        }
    }
    std::vector<SynapseGene> newSynapses;
    std::vector<NeuronGene> newNeurons;

    std::vector<int> IDs;

    std::vector<SynapseGene>::iterator momSynIt = mom.Synapses()->begin();
    std::vector<SynapseGene>::iterator dadSynIt = dad.Synapses()->begin();

    SynapseGene synToAdd;

    while(!(momSynIt==mom.Synapses()->end() && dadSynIt==dad.Synapses()->end()))//through dad and mom
    {
        //std::cout<<"MOM: "<<momSynIt->linkFrom<<" "<<momSynIt->linkTo<<" "<<
        //I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo)<<std::endl;
        //std::cout<<"DAD: "<<dadSynIt->linkFrom<<" "<<dadSynIt->linkTo<<" "<<
        //I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo)<<std::endl;

        if(momSynIt==mom.Synapses()->end() && dadSynIt!=dad.Synapses()->end() )//end of mom
        {
            if(best==DAD)
            {
                synToAdd=*dadSynIt;
                //std::cout<<"ga::crossover::Adding link "<<
                //synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                 //"(end of moms synapses)"<<std::endl;
            }
            ++dadSynIt;
        }
        else if(momSynIt!=mom.Synapses()->end() && dadSynIt==dad.Synapses()->end())//end of dad
        {
            if(best==MOM)
            {
                synToAdd=*momSynIt;
                //std::cout<<"ga::crossover::Adding link "<<
                  //       synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                    //   "(end of dads synapses)"<<std::endl;
            }
            ++momSynIt;
        }
        else if(I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo) > I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo))//mom high innov and best
        {
            if(best==MOM)
            {
                synToAdd=*momSynIt;
                 //std::cout<<"ga::crossover::Adding link "<<
                   //       synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                    //    "(moms innov is less and mom best)"<<std::endl;
            }
            ++momSynIt;
        }
        else if(I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo) < I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo))//dad high innov and best
        {
            if(best==DAD)
            {
                synToAdd=*dadSynIt;
                //std::cout<<"ga::crossover::Adding link "<<
                  //       synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                    //   "(dads innov is less and dad best)"<<std::endl;
            }
            ++dadSynIt;
        }
        else if(I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo) == I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo))//same iinovs
        {
            //random syn to add from mom or dad?
            if(RandINT(0,100) >= 50)
            {
                synToAdd=*dadSynIt;
            }
            else
            {
                synToAdd=*momSynIt;
            }
            ++dadSynIt;
            ++momSynIt;
        }

        // std::cout<<"NEW SYNAPSE "<<synToAdd.linkFrom<<" "<<synToAdd.linkTo<<std::endl;

        if(newSynapses.size() == 0 )
        {
            if( synToAdd.ID>=0)
            {
               // std::cout<<"NEW SYNAPSE added"<<synToAdd.linkFrom<<" "<<synToAdd.linkTo<<std::endl;
                newSynapses.push_back(synToAdd);
            }
        }
        else
        {
            if(newSynapses[newSynapses.size()-1] != synToAdd)
            {
                //std::cout<<"NEW SYNAPSE added"<<synToAdd.linkFrom<<" "<<synToAdd.linkTo<<std::endl;
                newSynapses.push_back(synToAdd);
            }
        }
    }
    Genome g(&I);

    g.SetSynapses(newSynapses);
    //adding neurons
    g.SetNeurons(*mom.Neurons());

    for(unsigned int i=0 ; i<dad.Neurons()->size() ; i++)
    {
        if(!g.neuronExists(dad.Neurons()->at(i).ID))
        {
            g.AddNeuron(dad.Neurons()->at(i));
        }
    }
    g.sortGenes();
    g.sortNeurons();
    //g.showInfo();
    return g;
}

Genome Ga::crossover2(Genome &mom, Genome &dad, Innovations &I)
{
    mom.sortGenes();
    mom.sortNeurons();
    dad.sortGenes();
    dad.sortNeurons();
    //std::cout<<"ga::crossover::CrossOVER start:"<<std::endl;
    parent_type best;
    if(mom.Fitness() == dad.Fitness())
    {
        if(mom.numGenes()==dad.numGenes())
        {
            //std::cout<<"ga::crossover::same fitness, same number of genes random best"<<std::endl;
            best = (parent_type)RandINT(0,1);
        }
        else
        {
            if(mom.numGenes() < dad.numGenes())
            {
                //std::cout<<"ga::crossover::BEST IS MOM(by genes num)"<<std::endl;
                best=MOM;
            }
            else
            {
                //std::cout<<"ga::crossover::BEST IS DAD(by genes num)"<<std::endl;
                best=DAD;
            }
        }
    }
    else //moms fitness != dads fitness
    {
        if(mom.Fitness()>dad.Fitness())
        {
            //std::cout<<"ga::crossover::BEST IS MOM(by fitness)"<<std::endl;
            best = MOM;
        }
        else
        {
            //std::cout<<"ga::crossover::BEST IS DAD(by fitness)"<<std::endl;
            best = DAD;
        }
    }
    std::vector<SynapseGene> newSynapses;
    std::vector<NeuronGene> newNeurons;

    std::vector<int> IDs;

    std::vector<SynapseGene>::iterator momSynIt = mom.Synapses()->begin();
    std::vector<SynapseGene>::iterator dadSynIt = dad.Synapses()->begin();

    SynapseGene synToAdd;

    while(!(momSynIt==mom.Synapses()->end() && dadSynIt==dad.Synapses()->end()))//through dad and mom
    {
        //std::cout<<"MOM: "<<momSynIt->linkFrom<<" "<<momSynIt->linkTo<<" "<<
        //I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo)<<std::endl;
        //std::cout<<"DAD: "<<dadSynIt->linkFrom<<" "<<dadSynIt->linkTo<<" "<<
        //I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo)<<std::endl;

        if(momSynIt==mom.Synapses()->end() && dadSynIt!=dad.Synapses()->end() )//end of mom
        {
            if(best==DAD)
            {
                synToAdd=*dadSynIt;
                //std::cout<<"ga::crossover::Adding link "<<
                //synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                 //"(end of moms synapses)"<<std::endl;
            }
            ++dadSynIt;
        }
        else if(momSynIt!=mom.Synapses()->end() && dadSynIt==dad.Synapses()->end())//end of dad
        {
            if(best==MOM)
            {
                synToAdd=*momSynIt;
                //std::cout<<"ga::crossover::Adding link "<<
                  //       synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                    //   "(end of dads synapses)"<<std::endl;
            }
            ++momSynIt;
        }
        else if(I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo) > I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo))//mom high innov and best
        {
            if(best==MOM)
            {
                synToAdd=*momSynIt;
                 //std::cout<<"ga::crossover::Adding link "<<
                   //       synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                    //    "(moms innov is less and mom best)"<<std::endl;
            }
            ++momSynIt;
        }
        else if(I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo) < I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo))//dad high innov and best
        {
            if(best==DAD)
            {
                synToAdd=*dadSynIt;
                //std::cout<<"ga::crossover::Adding link "<<
                  //       synToAdd.linkFrom<<" "<<synToAdd.linkTo<<
                    //   "(dads innov is less and dad best)"<<std::endl;
            }
            ++dadSynIt;
        }
        else if(I.getInnovation(momSynIt->linkFrom,momSynIt->linkTo) == I.getInnovation(dadSynIt->linkFrom,dadSynIt->linkTo))//same iinovs
        {
            //random syn to add from mom or dad?
            if(RandINT(0,100) >= 50)
            {
                synToAdd=*dadSynIt;
            }
            else
            {
                synToAdd=*momSynIt;
            }
            ++dadSynIt;
            ++momSynIt;
        }

        // std::cout<<"NEW SYNAPSE "<<synToAdd.linkFrom<<" "<<synToAdd.linkTo<<std::endl;

        if(newSynapses.size() == 0 )
        {
            if( synToAdd.ID>=0)
            {
               // std::cout<<"NEW SYNAPSE added"<<synToAdd.linkFrom<<" "<<synToAdd.linkTo<<std::endl;
                newSynapses.push_back(synToAdd);
            }
        }
        else
        {
            if(newSynapses[newSynapses.size()-1] != synToAdd)
            {
                //std::cout<<"NEW SYNAPSE added"<<synToAdd.linkFrom<<" "<<synToAdd.linkTo<<std::endl;
                newSynapses.push_back(synToAdd);
            }
        }
    }
    Genome g(&I);

    g.SetSynapses(newSynapses);
    //adding neurons
    g.SetNeurons(*mom.Neurons());

    for(unsigned int i=0 ; i<dad.Neurons()->size() ; i++)
    {
        if(!g.neuronExists(dad.Neurons()->at(i).ID))
        {
            g.AddNeuron(dad.Neurons()->at(i));
        }
    }
    g.sortGenes();
    g.sortNeurons();
    //g.showInfo();
    return g;
}

double Ga::deltaOfSpecies(Genome &one, Genome &two)
{
    //std::cout<<"DELTA OF SPECIES START"<<std::endl;
    //std::cout<<one.Id()<<" "<<two.Id()<<std::endl;
    double delta = 0;
    one.sortGenes();
    two.sortGenes();
    std::vector<SynapseGene>::iterator oneSynIt = one.Synapses()->begin();
    std::vector<SynapseGene>::iterator twoSynIt = two.Synapses()->begin();
    int excessGenes=0;
    int disjointGenes=0;
    double weightDifference=0;

    while(!(oneSynIt==one.Synapses()->end() && twoSynIt==two.Synapses()->end()))//through one and two
    {
        if(oneSynIt == one.Synapses()->end() && twoSynIt!=two.Synapses()->end())
        {
            excessGenes++;
            twoSynIt++;
        }
        else if(oneSynIt != one.Synapses()->end() && twoSynIt==two.Synapses()->end())
        {
            excessGenes++;
            oneSynIt++;
        }
        else
        {
            if(oneSynIt->ID != twoSynIt->ID)
            {
                disjointGenes++;

                if(oneSynIt->ID < twoSynIt->ID)
                    oneSynIt++;
                else
                    twoSynIt++;
            }
            else
            {
                weightDifference+=fabs(oneSynIt->weight - twoSynIt->weight);
                oneSynIt++;
                twoSynIt++;
            }
        }
    }

    int maxSize=0;
    if(one.Synapses()->size() > two.Synapses()->size())
    {
        maxSize = one.Synapses()->size();
    }
    else if(one.Synapses()->size() < two.Synapses()->size())
    {
        maxSize = two.Synapses()->size();
    }
    if(maxSize == 0)
    {
        return 0;
    }
    else
    {
        delta = (params::excessGeneCoefficient * excessGenes)/maxSize +
                (params::disjointedGeneCoefficient * disjointGenes)/maxSize +
                params::weightCoefficient * weightDifference;
        //std::cout<<"excess:"<<(params::excessGeneCoefficient * excessGenes)/maxSize<<std::endl;
        //std::cout<<"disjointed:"<<(params::disjointedGeneCoefficient * disjointGenes)/maxSize<<std::endl;
        //std::cout<<"weight:"<<params::weightCoefficient * weightDifference<<std::endl;
        //std::cout<<"DELTA:"<<delta<<std::endl;
        //std::cout<<"DELTA OF SPECIES end"<<std::endl;
        //std::cin.get();
        return delta;
    }
}

Genome *Ga::getGenome(int i)
{
    return &Genomes[i];
}

std::vector<Genome> Ga::createFirstPopulation(int numberOfSpecies, int inputs, int outputs, Innovations &I)
{
    std::cout<<"Creating 1st pop"<<std::endl;
    std::vector<Genome> genomes;
    for(int i =0 ; i<numberOfSpecies ; i++)
    {
        Genome g = Genome(i,inputs,outputs,&I);
        g.randomizeWeights();
        g.SetFitness(0);
        g.CreatePhenotype(1);
        genomes.push_back(g);
    }
    genomes = genomes;
    std::cout<<"1st pop created"<<std::endl;
    nextGenomeId=genomes.size();
    return genomes;
}

std::vector<Genome> Ga::createFirstPopulationNoLinks(int numberOfSpecies, int inputs, int outputs, Innovations &I)
{
    std::cout<<"Creating 1st pop"<<std::endl;
    std::vector<Genome> genomes;
    for(int i =0 ; i<numberOfSpecies ; i++)
    {
        Genome g = Genome(&I);
        g.SetId(i);
        for(unsigned int j=0 ; j<inputs ; j++)
        {
            g.AddNeuron(NeuronGene(j,input,false,j,0));
        }
        for(unsigned int j=inputs ; j<outputs+inputs ; j++)
        {
            g.AddNeuron(NeuronGene(j,output,false,j,1));
        }
        g.CreatePhenotype(1);
        g.SetFitness(0);
        genomes.push_back(g);
    }
    genomes = genomes;
    std::cout<<"1st pop created"<<std::endl;
    nextGenomeId=genomes.size();
    return genomes;
}

std::vector<NeuralNet> Ga::Epoch2(const std::vector<double> &fitnessScores)
{
    //std::cout<<"====================START OF EPOCH==================="<<std::endl;
    if(Genomes.size() != fitnessScores.size())
    {
        std::cout<<"Error: GenAlg::Epoch number of scores doesn't match number of genomes" << std::endl;
        std::cout<<"GenomesSize:"<<Genomes.size()<<" scoresSize:"<<fitnessScores.size()<<std::endl;
    }
    speciesPurge();
    updateScores(fitnessScores);
    // keep in mind theGenomes vector will be sorted ascendingly after the next step
    updateBestGenomes();
    SpeciateGenomes();
    UpdateSpeciesFitness();
    CalculateSpeciesSpawnAmounts();
    showInfo();
    std::cin.get();
    createNewPopulation();
    ++generation;
   // std::cout<<"====================END OF EPOCH==================="<<std::endl;

    return CreateNeuralNetworks();
}

void Ga::sortGenomes()
{
    //std::cout<<"--SortGENOMES START"<<std::endl;

    //SORTING GENOMES
    for(unsigned int i=0 ; i<Genomes.size() ;i++)
    {
        for(unsigned int j=i ; j<Genomes.size();j++)
        {
            if(Genomes[j] > Genomes[i])
            {
                Genome temp = Genomes[i];
                Genomes[i] = Genomes[j];
                Genomes[j] = temp;
            }
        }
    }
}

/*void Ga::SortAndRecord()
{
    std::cout<<"SortAndRecord START"<<std::endl;

    //SORTING SPECIES GENOMES
    for(unsigned int i=0 ; i<species.size() ; i++ )
    {
        species[i].Sort();
        if(species[i].vecMembers.size() != 0 )
        {
            if(species[i].vecMembers[0].fitness > species[i].leader.fitness)
            {
                species[i].noImprovementTime = 0;
                //Genome temp = species[i].leader;
                species[i].leader = species[i].vecMembers[0];
                species[i].vecMembers.erase(species[i].vecMembers.begin());
            }
        }
        species[i].showInfo();
    }
    std::cout<<"SortAndRecord END"<<std::endl;
}*/


void Ga::showInfo()
{
    std::cout<<std::endl<<"**************************SHOWINFO****************************"<<std::endl;
    std::cout<<"Generation: "<<generation<<std::endl;
    std::cout<<"bestEverFitness: "<<bestEverFitness<<std::endl;

    for(unsigned int i =0 ;i<Genomes.size() ; i++)
    {
        std::cout<<"Genome:"<<Genomes[i].Id()<<" Fitness:"<<Genomes[i].Fitness()<<
                   " AdjustedFitness:"<<Genomes[i].AdjustedFitness()<<" AmountToSpawn:"<<Genomes[i].AmountToSpawn()<<std::endl;
    }
    for(unsigned int i=0 ; i<species.size() ; i++)
    {
        std::cout<<"__________________________________________________________"<<std::endl;

        species[i].showInfo();
    }
    std::cout<<"**************************************************************"<<std::endl<<std::endl;
}

void Ga::showUI()
{
    std::cout<<"GENERATION:"<<generation<<std::endl;
    std::cout<<"Best fitness achieved:"<<bestEverFitness<<std::endl<<std::endl;
    std::cout<<"Number of gennomes:"<<Genomes.size()<<std::endl;
    std::cout<<"Number of species:"<<species.size()<<std::endl<<std::endl;
    //std::cout<<bestGenomes[0].Id()<<std::endl;
    bestGenomes[0].showInfo();
    double fitness = 0;
    std::vector<double> inputs = {0,0};
    std::vector<double> answ = bestGenomes[0].Phenotype()->Update(inputs,snapshot);
    fitness+= testXorFunction(std::vector<int>({0,0}),answ[0]);
    std::cout<<"inputs:0 0 "<<answ[0]<<std::endl;
    inputs = {0,1};
    answ = bestGenomes[0].Phenotype()->Update(inputs,snapshot);
    fitness+= testXorFunction(std::vector<int>({0,1}),answ[0]);
    std::cout<<"inputs:0 1 "<<answ[0]<<std::endl;
    inputs = {1,0};
    answ = bestGenomes[0].Phenotype()->Update(inputs,snapshot);
    fitness+= testXorFunction(std::vector<int>({1,0}),answ[0]);
    std::cout<<"inputs:1 0 "<<answ[0]<<std::endl;
    inputs = {1,1};
    answ = bestGenomes[0].Phenotype()->Update(inputs,snapshot);
    fitness+= testXorFunction(std::vector<int>({1,1}),answ[0]);
    std::cout<<"inputs:1 1 "<<answ[0]<<std::endl;
}

/*void Ga::clearScores()
{
    for(unsigned int i =0 ; i<species.size() ; i++)
    {
        species[i].clearScores();
    }
    for(unsigned int i =0 ; i<Genomes.size() ; i++)
    {
        Genomes[i].fitness = 0;
        Genomes[i].adjustedFitness = 0;
        Genomes[i].amountToSpawn=0;
    }
}
*/

/*
void Ga::optimizeSpecies()
{
    for(unsigned int i=0 ; i<species.size() ; i++)
    {
        for(unsigned int j=i+1 ; j<species.size() ; j++)
        {
            if(species[i].leader == species[j].leader)
            {
                std::cout<<"FOUND SAME SPECIES to erase"<<std::endl;
                species.erase(species.begin() + j);
            }
        }
    }
    sortSpecies();
    for(unsigned int i=0 ; i<species.size() ; i++)
    {

        if(species[i].noImprovementTime >= params::noImprovementTime)
        {
            if(i==0)//if the best performing..
            {
                species[i].purge();
                species[i].noImprovementTime=0;
            }
            else
            {
                 species.erase(species.begin()+i);
            }
        }

    }
}*/

Genome &Ga::tournamentSelection(int N)
{
    double bestFitness=0;
    int chosenOne = 0;
    for(int i=0 ; i<N ; i++)
    {
        int thisTry = RandINT(0,Genomes.size()-1);
        if(Genomes[thisTry].Fitness() > bestFitness)
        {
            chosenOne=thisTry;
            bestFitness = Genomes[thisTry].Fitness();
        }
    }
    return Genomes[chosenOne];
}

void Ga::speciesPurge()
{
    //std::cout<<"Species purge"<<std::endl;
    for(unsigned int i =0 ; i<species.size() ; i++)
    {
        species[i].purge();
        if(species[i].NoImprovementTime() > params::noImprovementTime && species[i].Leader()->Fitness() <= bestEverFitness)
        {
            //species[i].SetNoImprovementTime(0);
            species.erase(species.begin()+i);
        }
    }
}

void Ga::updateScores(const std::vector<double> &fitnessScores)
{
    //std::cout<<"Update scores"<<std::endl;
    for(unsigned int i = 0; i < Genomes.size(); ++i)
    {
        Genomes[i].SetFitness(fitnessScores[i]);
    }
    sortGenomes();
}

void Ga::updateBestGenomes()
{
    //std::cout<<"Updating best genomes"<<std::endl;
    bestGenomes.clear();
    bestEverFitness = bestEverFitness>Genomes[0].Fitness() ? bestEverFitness : Genomes[0].Fitness();

    for(unsigned int i = 0; i < params::numBestGenomes; ++i)
    {
        bestGenomes.push_back(Genomes[i]);
    }
}

void Ga::SpeciateGenomes()
{
    //std::cout<<"Speciation of genomes"<<std::endl;
    bool added = false;
    //auto compatibility_threshold = m_params.CompatibilityThreshold();
    for(auto& genome : Genomes)
    {
        for(auto& specie : species)
        {
            double diff_score = deltaOfSpecies(*specie.Leader(),genome);

            if(diff_score <= params::similarDeltaCoefficient)
            {
                specie.addMember(genome);
                genome.SetSpecie(specie.Id());
                added = true;
                break;
            }
        }
        if(!added)
        {
            Species newSp(genome,nextSpeciesId++);
            //Species new_species(genome, nextSpec++, &m_params);
            genome.SetSpecie(newSp.Id());

            species.push_back(newSp);
        }
        added = false;
    }
}

void Ga::UpdateSpeciesFitness()
{
    //std::cout<<"UpdateSpeciesFitness Start"<<std::endl;
    for(auto& s : species)
    {
        s.adjustFitnesses();
    }
}

void Ga::CalculateSpeciesSpawnAmounts()
{
    //std::cout<<"CalculateSpeciesSpawnAmounts Start"<<std::endl;
    for(auto& s : species)
    {
        s.CalculateSpawnAmount();
    }
}

void Ga::createNewPopulation()
{
    //std::cout<<"Creating new population"<<std::endl;
    std::vector<Genome> new_pop;
    auto population_size = params::PopulationSize;

    Genome baby;

    /*
    for(auto& g : bestGenomes)
    {
        new_pop.push_back(g);
    }
   */
    auto total_num_spawned = new_pop.size();
   // showInfo();
    for(auto& specie : species)
    {
        // break out if the population is large enough
        if(total_num_spawned >= population_size) break;

        bool leader_taken = false;
        int species_num_to_spawn = round(specie.NumToSpawn());
        if(specie.Leader()->NumGenes() == 0)
            leader_taken=true;
        while(species_num_to_spawn--)
        {
            if(!leader_taken)
            {
                baby = *specie.Leader();
               // std::cout<<"---leaderTaken::ID: "<<baby.Id()<<std::endl;
                leader_taken = true;
            }
            else
            {
                if(specie.Size() == 1)
                {
                    baby = specie.Spawn();
                    baby.SetId(nextGenomeId++);
                   // std::cout<<"---onlyOneExistingBabyTaken::ID: "<<baby.Id()<<std::endl;
                }
                else if(specie.Size() > 1)
                {
                    baby = MakeCrossoverBaby(specie, nextGenomeId++, baby);
                   // std::cout<<"---crossover::ID: "<<baby.Id()<<std::endl;

                }
                else
                {
                    baby = specie.Spawn();
                    baby.SetId(nextGenomeId++);
                }

                if((unsigned int)baby.Neurons()->size() < params::maxPermittedNeurons)
                {
                    baby.addNeuron(params::neuronMutationChance,5);
                }

                baby.addLink(params::linkMutationChance,params::reccurentChance,5,5);
                baby.mutateWeights(params::weightMutationChance, params::chanceOfWeightChange , params::weightMaxPerturbation);
                baby.mutateActivationResponse(params::NeuronActivationMutationProbability,params::NeuronActivationPerturbation);
                //baby.mutateSynapseAvailableness(0.1);
                //baby.deleteLink(0.25);

            }
            baby.sortGenes();
            //baby.showInfo();
            new_pop.push_back(baby);
            ++total_num_spawned;

            if(total_num_spawned >= population_size)
            {
                break;
            }
        }
        //std::cin.get();
    }
    //std::cin.get();

    if(new_pop.size() < population_size)
    {
        auto rqrd = population_size - new_pop.size();
        while(rqrd > 0)
        {
            new_pop.push_back(tournamentSelection(Genomes.size() / 5));
            --rqrd;
        }
    }
    //system("clear");
    //showInfo();
    //std::cin.get();
    Genomes = new_pop;

}

Genome Ga::MakeCrossoverBaby(Species &s, int next_id, Genome &prev_baby)
{
    Genome baby;
    Genome mom = s.Spawn();

    if(fRand(0,1) < params::CrossoverRate)
    {
        // find dad
        int num_attempts = 5;
        Genome dad = s.Spawn();
        //std::cout<<"crossover:"<<dad.Id()<<" "<<prev_baby.Id()<<std::endl;
        while(dad.Id() == mom.Id() && num_attempts--)
        {
            dad = s.Spawn();
        }

        if(dad.Id() != mom.Id())
        {
            baby = crossover(mom,dad,innovations);
        }
        else
        {
            baby = prev_baby;
        }
    }
    else
    {
        baby = mom;
    }

    baby.SetId(next_id);
    return baby;
}

std::vector<NeuralNet> Ga::CreateNeuralNetworks()
{
    std::vector<NeuralNet> newPhenotypes;
    for(unsigned int i=0 ; i<Genomes.size(); i++)
    {
        NeuralNet phenotype = *Genomes[i].CreatePhenotype(1);
        Genomes[i].SetPhenotype(phenotype);
        newPhenotypes.push_back(phenotype);
    }
    return newPhenotypes;
}

void Ga::work(unsigned int numGenerations)
{
    Genomes = createFirstPopulation(params::PopulationSize,2,1,innovations);
    //Genomes = createFirstPopulationNoLinks(params::PopulationSize,2,1,innovations);
    for(Genome& g : Genomes)
    {
        g.CreatePhenotype(1);
        //g.showInfo();
    }
    std::vector<double> fitnessScores;

    while(numGenerations--)
    {
        for(Genome& g : Genomes)
        {
            double fitness;
            /*for(int i=0 ; i<10;i++)
            {
                std::vector<double> inputs = {fRand(0,1)};
                std::vector<double> answ = g.phenotype->Update(inputs,snapshot);
                fitness += log10(testFitnessFunction(inputs[0],answ[0]) + 10);
                //std::cout<<"ID:"<<g.ID<<" input:"<<inputs[0]<<" answer:"<<answ[0]<<" fitness:"<<fitness<<std::endl;
                if(inputs[0] > 0.5 && answ[0] >= 0.8)
                {
                    percentage+=1;
                }
                else if(inputs[0] < 0.5 && answ[0] <= 0.2)
                {
                    percentage+=1;
                }
            }*/
            //fitness /= 10; //srednee arifmeticheskoye
            //rightPercentage.push_back(percentage / 10 * 100);
            //percentage=0;
            std::vector<double> inputs = {0,0};
            std::vector<double> answ = g.Phenotype()->Update(inputs,snapshot);
            fitness+= testXorFunction(std::vector<int>({0,0}),answ[0]);
           // std::cout<<"inputs:0 0 "<<answ[0]<<std::endl;

            inputs = {0,1};
            answ = g.Phenotype()->Update(inputs,snapshot);
            fitness+= testXorFunction(std::vector<int>({0,1}),answ[0]);
            //std::cout<<"inputs:0 1 "<<answ[0]<<std::endl;


            inputs = {1,0};
            answ = g.Phenotype()->Update(inputs,snapshot);
            fitness+= testXorFunction(std::vector<int>({1,0}),answ[0]);
           // std::cout<<"inputs:1 0 "<<answ[0]<<std::endl;

            inputs = {1,1};
            answ = g.Phenotype()->Update(inputs,snapshot);
            fitness+= testXorFunction(std::vector<int>({1,1}),answ[0]);
           // std::cout<<"inputs:1 1 "<<answ[0]<<std::endl;

           // std::cout<<"FITNESS:"<<fitness<<std::endl;
            g.SetFitness(fitness);
            fitnessScores.push_back(g.Fitness());
            fitness=0;
        }
        //std::cin.get();
        //system("clear");
        //showInfo();

        Epoch2(fitnessScores);
        fitnessScores.clear();
        showUI();
        //std::cin.get();
        //system("clear");
        //showUI();
        //std::cin.get();
       // std::cout<<"/////////////////// EPOCH END /////////////////////"<<std::endl;
    }

    //showInfo();
    //system("clear");
    //showUI();
    sortGenomes();

    /*
    for(auto& g : bestGenomes)
    {
        std::cout<<g.Id()<<std::endl;
        g.showInfo();
        double fitness;
        std::vector<double> inputs = {0,0};
        std::vector<double> answ = g.Phenotype()->Update(inputs,snapshot);
        fitness+= testXorFunction(std::vector<int>({0,0}),answ[0]);
        std::cout<<"inputs:0 0 "<<answ[0]<<std::endl;

        inputs = {0,1};
        answ = g.Phenotype()->Update(inputs,snapshot);
        fitness+= testXorFunction(std::vector<int>({0,1}),answ[0]);
        std::cout<<"inputs:0 1 "<<answ[0]<<std::endl;


        inputs = {1,0};
        answ = g.Phenotype()->Update(inputs,snapshot);
        fitness+= testXorFunction(std::vector<int>({1,0}),answ[0]);
        std::cout<<"inputs:1 0 "<<answ[0]<<std::endl;

        inputs = {1,1};
        answ = g.Phenotype()->Update(inputs,snapshot);
        fitness+= testXorFunction(std::vector<int>({1,1}),answ[0]);
        std::cout<<"inputs:1 1 "<<answ[0]<<std::endl;

        std::cout<<"FITNESS:"<<fitness<<std::endl;
        g.SetFitness(fitness);
        fitnessScores.push_back(g.Fitness());
        fitness=0;
    }
    */
}

double testFitnessFunction(double input, double answer) // >50?
{
    if(input < 0.5)
    {
        if(answer==0)
        {
            answer+=0.0001;
        }
        else if(answer == 1)
        {
            return 0;
        }

        if(answer <= 0.2) //+
        {
            return (1/answer);
        }
        else //-
        {
            return 0;//((1/answer) - 1);
        }
    }
    else
    {
        if(answer==1)
        {
            answer-=0.0001;
        }
        else if(answer == 0)
        {
            return 0;
        }
        if(answer <= 0.8) //-
        {
            return 0;//(( 1 / (1 - answer)) - 1);
        }
        else //+
        {
            return (1 / (1 - answer));
        }
    }
}

double testXorFunction(std::vector<int> inputs, double answer)
{
    if(inputs[0] == 0 && inputs[1]==0)
    {
        return pow(1-answer, 2);
    }
    else if(inputs[0] == 0 && inputs[1]==1)
    {
        return pow(answer, 2);
    }
    else if(inputs[0] == 1 && inputs[1]==0)
    {
        return pow(answer, 2);
    }
    else if(inputs[0] == 1 && inputs[1]==1)
    {
        return pow(1-answer, 2);
    }
    return -1000;

}

double testANDFunction(std::vector<int> inputs, double answer)
{
    if(inputs[0] == 0 && inputs[1]==0)
    {
        return pow(1-answer, 2);
    }
    else if(inputs[0] == 0 && inputs[1]==1)
    {
       return pow(1-answer, 2);
    }
    else if(inputs[0] == 1 && inputs[1]==0)
    {
        return pow(1-answer, 2);
    }
    else if(inputs[0] == 1 && inputs[1]==1)
    {
        return pow(answer, 2);
    }
    return -1000;
}
