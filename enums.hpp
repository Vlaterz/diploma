#ifndef ENUMS_HPP
#define ENUMS_HPP
enum run_type{snapshot, active};

enum innov_type{
    new_neuron,
    new_link
};
enum gene_type{
    neuron,
    synapse
};

enum neuron_type{
    hidden,
    input,
    output,
    bias,
    unknown
};
enum parent_type
{
    MOM,
    DAD
};


#endif // ENUMS_HPP
