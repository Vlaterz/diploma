#include "genome.h"

double fRand(double fMin, double fMax) //Randomizing function
{
    double f = (double)rand() / RAND_MAX;
    double answer = fMin + f * (fMax - fMin);
    return answer;
}

int RandINT(int min, int max)
{
    if(( (max-min+1) + min) == 0)
        return 0;
    return rand()%(max-min+1) + min;
}

bool Genome::linkExists(int linkFrom, int linkTo) //ok
{
    for(unsigned int i=0 ; i<synapses.size() ; i++)
    {
        if(synapses[i].linkFrom == linkFrom && synapses[i].linkTo == linkTo)
        {
            return true;
        }
    }
    return false;
}

bool Genome::neuronExists(int ID) //ok
{
    for(unsigned int i=0 ; i<neurons.size() ; i++)
    {
        if(neurons[i].ID == ID)
        {
            return true;
        }
    }
    return false;
}

NeuronGene *Genome::getNeuronByID(int ID)//ok
{
    for(unsigned int i=0 ; i<neurons.size() ; i++)
    {
        if(neurons[i].ID == ID)
        {
            return &neurons[i];
        }
    }
    return NULL;
}

int Genome::getElementPosition(int neuronID)//ok
{
    for(unsigned int i=0 ; i<neurons.size();i++)
    {
        if(neurons[i].ID == neuronID)
            return i;
    }
    std::cout<<"NOT FOUND"<<std::endl;
  return -1;
}

int Genome::getLastSynapseID()
{
    int maxID=-1;
    for(unsigned int i=0 ; i<synapses.size() ; i++)
    {
        if(maxID<synapses[i].ID)
            maxID=synapses[i].ID;
    }
    return maxID+1;
}

int Genome::getLastNeuronID()
{
    int maxID=-1;
    for(unsigned int i=0 ; i<neurons.size() ; i++)
    {
        if(neurons[i].ID > maxID)
            maxID=neurons[i].ID;
    }
    return maxID+1;
}

int Genome::numGenes()
{
    return synapses.size();//+neurons.size();
}

bool Genome::alreadyHaveThisNeuronID(int neuronID) //ok
{
    for(unsigned int i = 0; i<neurons.size() ; i++)
    {
        if(neurons[i].ID == neuronID)
        {
            return true;
        }
    }
    return false;
}

Genome::Genome(Innovations *innov)//ok
    :ID(-1),specie(-1),fitness(0),adjustedFitness(0),amountToSpawn(0),numInputs(0),numOutputs(0),innovations(innov){}

Genome::Genome(int id, int inputs, int outputs, Innovations *innov)//ok
    :ID(id),specie(-1),fitness(0),adjustedFitness(0),amountToSpawn(0),numInputs(inputs),numOutputs(outputs),innovations(innov)
{
    //creating simple net with no hidden layer
    double input_row_slice = 1 / (double)(inputs+2);
    for(int i=0; i<inputs; i++)
    {
        bool newNeur=true;
        for(int j=0;j<innovations->getLastNeuronId()+1 ; j++) //all neuron IDs
        {
            if(alreadyHaveThisNeuronID(j)) //quit if already have this ID
            {
                newNeur=true;
                continue;
            }
            if(innovations->getInnovation(j,input) >= 0) //check if we have this ID
            {
                neurons.push_back(NeuronGene(j,input,false,(i+2)*input_row_slice,0));
                newNeur=false;
                break;
            }
            newNeur=true;
        }
        if(newNeur)
        {
            int newID = innovations->getLastNeuronId()+1;
            innovations->addInnovation(newID,input);
            neurons.push_back(NeuronGene(newID,input,false,(i+2)*input_row_slice,0));
        }
    }
    double output_row_slice = 1 / (double)(outputs+1);

    /*
    //bias
    int ID;
    int inn = innovations->getInnovation(innovations->getLastNeuronId()+1,bias);
    if(inn < 0)
    {
        innovations->addInnovation(innovations->getLastNeuronId()+1,bias);
         ID = innovations->getInnovation(innovations->getLastNeuronId()+1,bias);
    }
    neurons.push_back(NeuronGene(innovations->getLastNeuronId()+1,bias,false,-1,-1));
*/
    for(int i=0; i<outputs; i++)
    {
        bool newNeur=false;
        for(int j=0;j<innovations->getLastNeuronId()+1 ; j++) //all neuron IDs
        {
            if(alreadyHaveThisNeuronID(j)) //quit if already have this ID
            {
                newNeur=true;
                continue;
            }
            if(innovations->getInnovation(j,output) >= 0) //check if we have this ID
            {
                neurons.push_back(NeuronGene(j,output,false,(i+1)*output_row_slice,1));
                newNeur=false;
                break;
            }
            newNeur=true;
        }
        if(newNeur)
        {
            int newID = innovations->getLastNeuronId()+1;
            innovations->addInnovation(newID,output);
            neurons.push_back(NeuronGene(newID,output,false,(i+1)*output_row_slice,1));
        }
    }

    //adding links

    for(unsigned int i=0 ; i<neurons.size(); i++)
    {
        for(unsigned int j=0 ; j<neurons.size();j++)
        {
            if(neurons[i].ID != neurons[j].ID && neurons[i].type != neurons[j].type && neurons[i].type == input && neurons[i].type != bias)
            {
                int ID=innovations->getInnovation(neurons[i].ID,neurons[j].ID);
                if(ID >= 0)
                {
                    synapses.push_back(SynapseGene(neurons[i].ID,neurons[j].ID,fRand(-1,1),ID,false,true));
                }
                else
                {
                    innovations->addInnovation(neurons[i].ID,neurons[j].ID);
                    synapses.push_back(SynapseGene(neurons[i].ID,neurons[j].ID,fRand(-1,1),innovations->getInnovation(neurons[i].ID,neurons[j].ID),false,true));
                }
            }
        }
    }
}

bool Genome::addLink(double mutation_prob,//ok
                     double reccurentChance,
                     int NumTrysToFindLoop,
                     int NumTrysToAddLink)
{
    if(fRand(0,1) > mutation_prob )
    {
        return false;
    }
    int neuronID1 = -1;
    int neuronID2 = -1;  
    bool rec=false;

    if(fRand(0,1) < reccurentChance )
    {
        for(int i=0 ; i<NumTrysToFindLoop ; i++) //try to add loop
        {
            int neuronPos = RandINT(numInputs+1,neurons.size()-1);
            if(!neurons[neuronPos].recurrent &&
                    neurons[neuronPos].type != bias &&
                    neurons[neuronPos].type != input)
            {
                neuronID1 = neuronID2 = neurons[neuronPos].ID;
                neurons[neuronPos].recurrent = true;
                rec = true;
                NumTrysToFindLoop=0;
            }
        }
    }
    else
    {
        //std::cout<<"Genome::addlink - new link mutation!"<<std::endl;
        while(NumTrysToAddLink--)
        {
            NeuronGene neuron1=neurons[RandINT(0,neurons.size()-1)];
            NeuronGene neuron2=neurons[RandINT(0,neurons.size()-1)];
            if(neuron2.type == bias || neuron2.type == input || neuron1.type==output)
            {
                continue;
            }

            neuronID1 = neuron1.ID;
            neuronID2 = neuron2.ID;
            if( !linkExists(neuronID1,neuronID2) && (neuronID1!=neuronID2))
            {
                if(neurons[getElementPosition(neuronID1)].y > neurons[getElementPosition(neuronID2)].y )
                {
                    rec=true;
                }
                NumTrysToAddLink=0;
            }
            else
            {
                neuronID1=-1;
                neuronID2=-1;
            }
        }
    }
    if( neuronID1 < 0 || neuronID2 < 0)
    {
        return false;
    }
    else //two neurons to connect are found properly
    {
        //std::cout<<"ID:"<<ID<<"Genome::addlink - mutation!"<<std::endl;
        int newInnovId = innovations->getInnovation(neuronID1,neuronID2);//new innovation?
        if(newInnovId < 0) //no such innovation in global innovations
        {
            innovations->addInnovation(neuronID1,neuronID2);
            int innovID = innovations->getInnovation(neuronID1,neuronID2);
            synapses.push_back(SynapseGene(neuronID1,neuronID2,1,innovID,rec,true));
        }
        else
        {
            synapses.push_back(SynapseGene(neuronID1,neuronID2,1,newInnovId,rec,true));
        }
    }
    return true;
}

bool Genome::addNeuron(double mutation, int NumTrysToFindOldLink)//ok
{
    if(mutation < fRand(0,1))
    {
        return false;
    }

    bool niceLinkFound=false;
    int chosenLink=0;

    //first a link is chosen to split. If the genome is small the code makes
    //sure one of the older links is split to ensure a chaining effect does
    //not occur. Here, if the genome contains less than 5 hidden neurons it
    //is considered to be too small to select a link at random.
    const unsigned int sizeThreshold = numInputs+numOutputs+5;
    if(synapses.size() < sizeThreshold) //small net
    {
        for(int i = 0 ;i<NumTrysToFindOldLink ; i++)
        {
            if(synapses.size() == 0)
            {
                return false;
            }
            chosenLink = RandINT(0,synapses.size() - 1 - (int)sqrt(synapses.size()));
            int fromNeuron= synapses[chosenLink].linkFrom;

            if(synapses[chosenLink].enabled &&
                    !synapses[chosenLink].recurrent &&
                        getNeuronByID(fromNeuron)->type!=bias)
            {
                niceLinkFound=true;
                NumTrysToFindOldLink=0;
            }
        }
        if(!niceLinkFound)
        {
            std::cout<<"ID:"<<ID<<" Genome::addNeuron - mutation FAILED (failed to find a nice link)"<<std::endl;
        }
    }
    else //big nice net
    {
       while(!niceLinkFound)
       {
           chosenLink = RandINT(0,synapses.size()-1);
           int fromNeuron= synapses[chosenLink].linkFrom;

           if(synapses[chosenLink].enabled &&
                   !synapses[chosenLink].recurrent &&
                       getNeuronByID(fromNeuron)->type!=bias)
           {
               niceLinkFound=true;
           }
       }
    }
    synapses[chosenLink].enabled=false;
    double oldWeight = synapses[chosenLink].weight;
    int from = synapses[chosenLink].linkFrom;
    int to = synapses[chosenLink].linkTo;
    std::cout<<"LINK FOUND "<<chosenLink<<". FROM:"<<from<<" TO:"<<to<<std::endl;
    double new_depth = (neurons[getElementPosition(from)].y + neurons[getElementPosition(to)].y)/2;
    double new_width = (neurons[getElementPosition(from)].x + neurons[getElementPosition(to)].x)/2;
    int id = innovations->getInnovation(from,to);
    std::cout<<"ID OF INNOVATION:"<<id<<std::endl;
    std::cout<<"ID:"<<ID<<" Genome::addNeuron - mutation"<<std::endl;


    if(id<0)// no such synapse in innovations
    {
        std::cout<<"bool Genome::addNeuron(double mutation, int NumTrysToFindOldLink) FIND ME! :P"<<std::endl;
        std::cin.get();
        int neuronID=getLastNeuronID();
        if(innovations->getInnovation(neuronID,hidden) < 0)//neuron doesnot exists in innovations
            innovations->addInnovation(neuronID,hidden);
        neurons.push_back(NeuronGene(neuronID,hidden,false,new_width,new_depth));
        synapses.push_back(SynapseGene(from,neuronID,1,getLastSynapseID(),false,true));
        innovations->addInnovation(from,neuronID);
        synapses.push_back(SynapseGene(neuronID,to,synapses[chosenLink].weight,getLastSynapseID(),false,true));
        innovations->addInnovation(neuronID,to);
    }
    else //synapse exists in innovations
    {
        int neuronID=getLastNeuronID();
        if(innovations->getInnovation(neuronID,hidden)<0) //neuron doesnot exist
        {
            innovations->addInnovation(innovations->getLastNeuronId()+1,hidden);
            neuronID = innovations->getLastNeuronId();
        }
        int link1 = innovations->getInnovation(from,neuronID);
        int link2 = innovations->getInnovation(neuronID,to);
        if(link1<0)
        {
            innovations->addInnovation(from,neuronID);
            link1 = innovations->getInnovation(from,neuronID);
        }
        if(link2<0)
        {
            innovations->addInnovation(neuronID,to);
            link2 = innovations->getInnovation(neuronID,to);
        }
        if( (link1 < 0) || (link2 < 0) )
        {
            std::cout<<"CRITICAL ERROR"<<neuronID<<" "<<link1<<link2<<std::endl;
            innovations->showInfo();
            return false;
        }
        neurons.push_back(NeuronGene(neuronID,hidden,false,new_width,new_depth));
        synapses.push_back(SynapseGene(from,neuronID,1,link1,false,true));
        synapses.push_back(SynapseGene(neuronID,to,oldWeight,link2,false,true));
    }
    return true;
}

void Genome::addLink(int neur1, int neur2) //ok
{
    if(  !(neuronExists(neur1) && neuronExists(neur2))  )
    {
        std::cout<<"GEnome::addLInk2::ERROR, ONE OF NEURONS IS NOT FOUND"<<std::endl;
        return;
    }
   if(linkExists(neur1,neur2))
   {
       std::cout<<"GEnome::addLInk2::ERROR, LINK EXISTS"<<std::endl;
       return;
   }
   int id = innovations->getInnovation(neur1,neur2);
   if(id<0)
   {
       innovations->addInnovation(neur1,neur2);
       id=innovations->getInnovation(neur1,neur2);
   }
   synapses.push_back(SynapseGene(neur1,neur2,1,id,false,true));
}

void Genome::addNeuron(neuron_type type, double x, double y)//ok
{
    int ID = innovations->getInnovation(getLastNeuronID(),type);
    if(ID<0)
    {
        innovations->addInnovation(getLastNeuronID(),type);
        ID=innovations->getInnovation(getLastNeuronID(),type);
    }
    neurons.push_back(NeuronGene(innovations->getNeuronId(ID),type,false,x,y));
    return;
}

void Genome::addNeuron(neuron_type type, int ID, double x, double y)//ok
{
    int tempID = innovations->getInnovation(ID,type);
    if(tempID<0)
    {
        innovations->addInnovation(ID,type);
    }
    neurons.push_back(NeuronGene(ID,type,false,x,y));
    return;
}

bool Genome::mutateWeights(double probability , double newWeightProbability , double max_perturbation)//ok
{
    bool mutated = false;
    for(auto& synapse : synapses)
    {
        if(fRand(0,1) < probability)
        {
            mutated=true;
            if(fRand(0,1) < newWeightProbability)
            {
                synapse.weight = fRand(-1,1);
            }
            else
            {
                double perturbation = fRand(-1,1) * max_perturbation;
                synapse.weight +=perturbation;
            }
        }
    }
    return mutated;
}

void Genome::mutateActivationResponse(double mutation, double maxPerturbation)//ok
{
    for(auto& neur : neurons)
    {
        if(fRand(0,1) < mutation)
        {
            neur.activation += fRand(-1,1) * maxPerturbation;
        }
    }
}

void Genome::mutateSynapseAvailableness(double probability)//ok
{
    std::cout<<"mutate AVAILABLENESS"<<std::endl;
    for(auto& s : synapses)
    {
        if(fRand(0,1) < probability)
        {
            s.enabled += -1;
        }
    }
}

void Genome::deleteLink(double probability)
{
    for(auto& syn : synapses)
    {
        if(probability > fRand(0,1))
        {
         int syn = RandINT(0,synapses.size()-1);
         synapses.erase(synapses.begin()+syn);
        }
        else
        {
            return;
        }
    }
}

void Genome::showInfo()//ok
{

    std::cout<<"GenomE ID:"<<ID<<" FITNESS:"<<fitness<<std::endl;;
    for(unsigned int i=0 ; i<neurons.size() ; i++)
    {
        neurons[i].showInfo();
    }
    for(unsigned int i=0 ; i<synapses.size() ; i++)
    {
        synapses[i].showInfo();
    }
}

void Genome::setAdjFitness(double adjFit)//ok
{
    adjustedFitness=adjFit;
}

void Genome::sortGenes() //ok
{
    for(unsigned int i=0 ; i<synapses.size(); i++)
    {
        for(unsigned int j=i ; j< synapses.size(); j++)
        {
            if(synapses[j].ID < synapses[i].ID )
            {
                SynapseGene temp;
                temp = synapses[i];
                synapses[i]= synapses[j];
                synapses[j]=temp;
            }
        }
    }
}

void Genome::sortNeurons() //ok
{
        for(unsigned int i=0 ; i<neurons.size(); i++)
        {
            for(unsigned int j=i ; j< neurons.size(); j++)
            {
                if(neurons[j].ID < neurons[i].ID )
                {
                    NeuronGene temp;
                    temp = neurons[i];
                    neurons[i]= neurons[j];
                    neurons[j]=temp;
                }
            }
        }
}

void Genome::randomizeWeights()//ok
{
    for(unsigned int i =0 ; i<synapses.size(); i++)
    {
        synapses[i].weight = fRand(-1,1);
    }
}

NeuralNet *Genome::CreatePhenotype(int depth)//ok
{
    //deletePhenotype(); //LOOP ERROR?
    if(phenotype != NULL)
    {
        phenotype = NULL;
    }
    std::vector<Neuron*> vecNeurons;

    for(unsigned int i=0 ; i<neurons.size();i++)
    {
        Neuron* n=new Neuron(neurons[i].type,
                             neurons[i].ID,
                             neurons[i].y,
                             neurons[i].x,
                             neurons[i].activation);
        vecNeurons.push_back(n);
    }
    for(unsigned int i=0 ; i<synapses.size() ; i++)
    {
        if(synapses[i].enabled)
        {
            int element = getElementPosition(synapses[i].linkFrom);
            Neuron* fromNeuron = vecNeurons[element];

            element = getElementPosition(synapses[i].linkTo);
            Neuron* toNeuron = vecNeurons[element];
            Synapse tmpSyn(synapses[i].weight,
                           fromNeuron,toNeuron,
                           synapses[i].recurrent);
            fromNeuron->linksOut.push_back(tmpSyn);
            toNeuron->linksIn.push_back(tmpSyn);
        }
    }
    phenotype = new NeuralNet(vecNeurons,depth);
    return phenotype;
}

bool Genome::operator <(Genome right)//ok
{
    if(right.fitness > fitness)
        return true;
    else
        return false;
}

bool Genome::operator >(Genome right)//ok
{
    if(right.fitness < fitness)
        return true;
    else
        return false;
}

bool Genome::operator ==(Genome right)//ok
{
    if(ID == right.ID && fitness==right.fitness)
        return true;
    else
        return false;
}

Genome::~Genome()
{

}
