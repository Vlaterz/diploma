#ifndef GENOME_H
#define GENOME_H

#include <vector>
#include "neuronGene.h"
#include "synapseGene.h"
#include "innovation.h"
#include <math.h>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include "neuralnet.h"
#include "config.hpp"
#include <set>
class NeuralNet;

class Genome
{
    int ID;
    int specie;
    std::vector<NeuronGene> neurons;
    std::vector<SynapseGene> synapses;

    double fitness;
    double adjustedFitness;
    double amountToSpawn;

    int numInputs ;
    int numOutputs;

    Innovations* innovations;
    NeuralNet* phenotype;

public:
    Genome(){}
    Genome(Innovations* innov);
    Genome(int id, int inputs , int outputs, Innovations* innov);
    Genome(int id, std::vector<NeuronGene> neurons, std::vector<SynapseGene> links, int num_inputs, int num_outputs)
                            :ID(id),
                            neurons(neurons),
                            synapses(links),
                            fitness(0),
                            adjustedFitness(0),
                            amountToSpawn(0),
                            numInputs(num_inputs),
                            numOutputs(num_outputs){}

    bool linkExists(int linkFrom, int linkTo);
    bool neuronExists(int ID);
    bool alreadyHaveThisNeuronID(int neuronID);

    NeuronGene* getNeuronByID(int ID);
    int getElementPosition(int neuronID);
    int getLastSynapseID();
    int getLastNeuronID();

    int numGenes();

    bool addLink(double mutation_prob, double reccurentVhance, int NumTrysToFindLoop, int NumTrysToAddLink);
    bool addNeuron(double mutation, int  NumTrysToFindOldLink);
    bool mutateWeights(double probability, double newWeightProbability, double max_perturbation);
    void mutateActivationResponse(double mutation, double maxPerturbation);
    void mutateSynapseAvailableness(double probability);
    void deleteLink(double probability);

    void addLink(int neur1 , int neur2);
    void addNeuron(neuron_type type, double x, double y);
    void addNeuron(neuron_type type, int ID, double x, double y);

    void showInfo();
    void setAdjFitness(double adjFit);
    void sortGenes();
    void sortNeurons();
    void randomizeWeights();

    NeuralNet* CreatePhenotype(int depth);
    bool operator <(Genome right);
    bool operator >(Genome right);
    bool operator ==(Genome right);
//------------------Get methods----------------------
    double Fitness()const{return fitness;}
    double AdjustedFitness()const{return adjustedFitness;}
    double AmountToSpawn()const{return amountToSpawn;}
    int Id()const{return ID;}
    std::vector<NeuronGene>* Neurons (){return &neurons;}
    std::vector<SynapseGene>* Synapses(){return &synapses;}
    NeuralNet* Phenotype(){return phenotype;}
    int NumGenes(){return synapses.size();}
//--------------------Set methods--------------------
    void SetSpawnAmount(double spwnAm){amountToSpawn=spwnAm;}
    void SetSynapses(std::vector<SynapseGene> syns){synapses=syns;}
    void SetNeurons(std::vector<NeuronGene> neurs){neurons = neurs;}
    void SetFitness(double fit){fitness = fit;}
    void SetSpecie(int sp){specie=sp;}
    void SetId(int id){ID=id;}
    void SetPhenotype(NeuralNet net){*phenotype = net;}
    void AddNeuron(NeuronGene neur){neurons.push_back(neur);}

    //double GetCompared(const Genome& gen);
    //operator < ????
    // Genome(int id, std::vector<Neuron> elements, std::vector<Synapse> links, int inputs, int outputs);
    //Genome(const Genome& gen);
    //Genome& operator = (const Genome& gen);
    ~Genome();
};

int RandINT(int min, int max);
double fRand(double fMin, double fMax);

#endif // GENOME_H
