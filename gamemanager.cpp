#include "gamemanager.h"

GameManager::GameManager() : speed(-1.7),score(0)
{
    for(int i = 0 ; i<RandINT(3,7) ; i++)
    walls.push_back(wall(speed,500 + i * RandINT(100,300)));

    algo = new Ga;
    *algo->getGenomes() = algo->createFirstPopulation(params::PopulationSize,2,1,*algo->GetInnovations());
    //algo.createFirstPopulationNoLinks(params::PopulationSize,2,1,*algo.GetInnovations());
    genomes = algo->getGenomes();
    currentGenome=0;
    epochTime = false;
}

void GameManager::update(double time)
{
    score+=0.1;
   // std::cout<<score<<std::endl;
    hero.update(time);
    if(walls.empty())
    {
        for(int i = 0 ; i<RandINT(3,7) ; i++)
        walls.push_back(wall(speed,500 + i * RandINT(100,300)));
    }
    for(int i = 0 ; i<walls.size() ; i++)
    {
        walls[i].update(time);
        if(walls[i].rect.getPosition().x < 0)
        {
            walls.erase(walls.begin()+i);
            speed -=0.1;
        }
    }

    inputsForNN.push_back(walls[0].rect.getPosition().x-hero.rect.getPosition().x);
    inputsForNN.push_back(speed);

    std::vector<double> outputs;
    outputs = genomes->at(currentGenome).Phenotype()->Update(inputsForNN,snapshot);

    if(outputs[0] > 0.9 && !hero.inAir)
    {
      hero.ySpeed=-8;
    }
    system("clear");
    std::cout<<"GEn:"<<currentGenome<<" scoresSize:"<<scores.size()<<std::endl;
    genomes->at(currentGenome).showInfo();
    std::cout<<"inputs:"<<inputsForNN[0]<<" "<<inputsForNN[1]<<" output:"<<outputs[0]<<std::endl;

    collisionCheck();
    inputsForNN.clear();
    if(epochTime)
    {
        algo->Epoch2(scores);
        scores.clear();
        genomes=algo->getGenomes();
        epochTime = false;
        currentGenome = 0;
    }

}


void GameManager::collisionCheck()
{
    for(auto& w : walls)
    {
        if( w.rect.getPosition().x < (hero.rect.getPosition().x + hero.rect.getSize().x)
                && w.rect.getPosition().y < (hero.rect.getPosition().y + hero.rect.getSize().y ))
        {

            if(w.rect.getPosition().x+w.rect.getSize().x > hero.rect.getPosition().x )
            {
                scores.push_back(score);
                score = 0;
                speed = -2;
                walls.clear();
                if(currentGenome < genomes->size()-1)
                  currentGenome++;
                else
                    epochTime=true;
            }
        }
    }
}
