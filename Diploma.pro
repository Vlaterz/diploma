
TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -L"/home/vlater/MyCoding/SFML/SFMLRelease/lib"

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

INCLUDEPATH += "/home/vlater/MyCoding/SFML/SFMLRelease/include"
DEPENDPATH += "/home/vlater/MyCoding/SFML/SFMLRelease/include"

SOURCES += main.cpp \
    genome.cpp \
    innovation.cpp \
    gene.cpp \
    species.cpp \
    neuronGene.cpp \
    synapseGene.cpp \
    neuron.cpp \
    synapse.cpp \
    neuralnet.cpp \
    ga.cpp \
    config.cpp \
    object.cpp \
    hero.cpp \
    wall.cpp \
    gamemanager.cpp

HEADERS += \
    genome.h \
    innovation.h \
    enums.hpp \
    gene.h \
    species.h \
    config.hpp \
    neuronGene.h \
    synapseGene.h \
    neuron.h \
    synapse.h \
    neuralnet.h \
    ga.h \
    includes.hpp \
    object.h \
    hero.h \
    wall.h \
    gamemanager.h

DISTFILES += \
    fonnt.ttf
