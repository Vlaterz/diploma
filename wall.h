#ifndef WALL_H
#define WALL_H
#include <SFML/Graphics.hpp>
#include "object.h"
#include "iostream"
class wall:public object
{
public:
    sf::RectangleShape rect;
    double speed;

    wall(double Speed,double x);
    void update(float time);
};

#endif // WALL_H
