#include "wall.h"

wall::wall(double Speed, double x)
{
    speed = Speed;
    rect.setFillColor(sf::Color::Red);
    rect.setSize(sf::Vector2f(5,80));
    rect.setPosition(sf::Vector2f(x,430));
}

void wall::update(float time)
{
    rect.move(sf::Vector2f(time*speed,0));
}
