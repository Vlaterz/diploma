#ifndef NEURON_H
#define NEURON_H
class Synapse;
#include "iostream"
#include "synapse.h"
#include "vector"
#include "enums.hpp"
class Neuron
{
public:
    Neuron():activationResponse(1){}
    std::vector<Synapse> linksIn;
    std::vector<Synapse> linksOut;
    double sumActivation;
    double output;
    neuron_type NeuronType;
    int ID;
    int y;
    int x;
    double activationResponse;//krivizna sigmoidi
    Neuron(neuron_type type, int id, double y, double x, double ActResponse);
    void showInfo();
};

#endif // NEURON_H
