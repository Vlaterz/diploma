#include <iostream>
#include "ga.h"
//#include "species.h"
#include <SFML/Graphics.hpp>
#include <cstdlib>
#include "hero.h"
#include "gamemanager.h"
using namespace std;

int main()
{
    std::cout<<"START OF PROGRAMM!"<<std::endl;;
    std::srand(time(NULL));


    sf::Clock clock;
    float time = clock.getElapsedTime().asMicroseconds();
    clock.restart();
    float gameSpeed = 5;
    time = time/gameSpeed;
    GameManager manager;
    //Hero heroo;

    sf::RenderWindow window(sf::VideoMode(640, 480), "Genetic algorithm");
    while (window.isOpen())
        {
            sf::Event event;
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    window.close();
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                window.close();
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !manager.hero.inAir)
            {
                manager.hero.ySpeed=-10;
                //heroo.ySpeed = -1;
            }
            manager.update(time);
            window.clear(sf::Color::Black);
            window.draw(manager.hero.rect);
            for(auto& k : manager.walls)
            {
                window.draw(k.rect);
            }
            // end the current frame
            window.display();
        }


    //Ga algo;
   // Visualisation v;


/*
    Genome kek(algo.GetInnovations());
    Genome lol(algo.GetInnovations());
    kek.SetFitness(10);
    lol.SetFitness(20);
    kek.AddNeuron(NeuronGene(0,input,false,0,0));
    kek.AddNeuron(NeuronGene(1,input,false,1,0));
    kek.AddNeuron(NeuronGene(2,input,false,2,0));
    kek.AddNeuron(NeuronGene(3,output,false,1,1));
    kek.AddNeuron(NeuronGene(4,hidden,false,1,0.5));
    kek.addLink(0,3);
    kek.addLink(1,3);
    kek.Synapses()->at(1).enabled = 0;
    kek.addLink(2,3);
    kek.addLink(1,4);
    kek.addLink(4,3);
    kek.addLink(0,4);

    lol.AddNeuron(NeuronGene(0,input,false,0,0));
    lol.AddNeuron(NeuronGene(1,input,false,1,0));
    lol.AddNeuron(NeuronGene(2,input,false,2,0));
    lol.AddNeuron(NeuronGene(3,output,false,1,1));
    lol.AddNeuron(NeuronGene(4,hidden,false,1,0.5));
    lol.AddNeuron(NeuronGene(5,hidden,false,2.5,0.25));
    lol.addLink(0,3);
    lol.addLink(1,3);
    lol.Synapses()->at(1).enabled = 0;
    lol.addLink(2,3);
    lol.addLink(1,4);
    lol.addLink(4,3);
    lol.Synapses()->at(4).enabled = 0;
    lol.addLink(4,5);
    lol.addLink(5,3);
    lol.addLink(2,4);
    lol.addLink(0,5);

    algo.GetInnovations()->showInfo();

    Genome temp = algo.crossover(kek,lol,*algo.GetInnovations());
    temp.showInfo();

    algo.GetInnovations()->showInfo();
*/
   // algo.work(2000);
std::cout<<"----------------------------------------------------------"<<std::endl;
    /*
    std::vector<double> scores;
    for(int i=0 ; i<100 ; i++)
    {
        scores.push_back(fRand(0,100));
    }
    auto temp = algo.Epoch(scores);
    */

    //----------------------------------------------------------

    /*
    std::vector<Species*> sp;
    sp.push_back(new Species(*genomes[0],0));
    for(unsigned int i=0 ; i<genomes.size() ; i++)
    {
        bool newSp=true;
        for(unsigned int j=0 ; j<sp.size() ; j++)
        {
            Genome leader=sp[j]->Leader();
            if(algo.deltaOfSpecies(leader,*genomes[i]) <= 3)
            {
                sp[j]->addMember(*genomes[i]);
                newSp=false;
            }
        }
        if(newSp)
        {
            sp.push_back(new Species(*genomes[i],sp.size()+1));
        }
    }
    algo.species = sp;
    */
    /*
    Genome g1(&I);
    Genome g2(&I);
    g1.addNeuron(input,0,0);
    g1.addNeuron(input,1,0);
    g1.addNeuron(input,2,0);
    g1.addNeuron(output,1,10);
    g1.addNeuron(hidden,1,3);
    g1.addNeuron(hidden,1,6);
    g1.addLink(0,3);
    g1.addLink(1,3);
    g1.synapses[1].enabled=false;
    g1.addLink(2,3);
    g1.synapses[2].enabled=false;
    g1.addLink(1,4);
    g1.addLink(4,3);
    g1.synapses[4].enabled=false;
    g1.addLink(2,5);
    g1.addLink(4,5);
    g1.addLink(5,3);
    g2.addNeuron(input,0,0);
    g2.addNeuron(input,1,0);
    g2.addNeuron(input,2,0);
    g2.addNeuron(output,1,10);
    g2.addNeuron(hidden,1.5,5);
    g2.addLink(0,3);
    g2.synapses[0].enabled=false;
    g2.addLink(1,3);
    g2.addLink(2,3);
    g2.synapses[2].enabled=false;
    g2.addLink(4,3);
    g2.addLink(0,4);
    g2.addLink(2,4);
    g1.showInfo();
    g2.showInfo();
    deltaOfSpecies(g1,g2);
    g1.fitness=2;
    g2.fitness=1;
    Genome kek = crossover(g1,g2,I);
    kek.showInfo();
*/
    //I.showInfo();
    /*
    std::cout<<"sp.size = "<<sp.size()<<endl;
    for(unsigned int i=0 ; i<sp.size(); i++)
    {
        std::cout<<sp[i]->NumMembers()<<" ";
    } std::cout<<std::endl;
   */

    /*
    std::cout<<endl<<"NN creation..."<<std::endl;
    std::vector<double> ttt;
    ttt.push_back(14.1);
    ttt.push_back(67.1);
    ttt.push_back(0);
    for(int i = 0 ; i<genomes.size() ;i++)
    {
        NeuralNet* test =  genomes[i]->CreatePhenotype(1);
        ttt=test->Update(ttt,snapshot);
        std::cout<<ttt[0]<<" "<<ttt[1]<<" "<<ttt[2]<<std::endl;
    }
    */

    std::cout<<endl<<"EOPROGRAMM"<<std::endl;
    return 0;

    //niching ftp://ftp.vt.tpu.ru/study/Spitsyn/public/from%20Tzoy/Neuroevolution/lectures/lec9.pdf
}
