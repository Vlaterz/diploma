#include "neuron.h"



Neuron::Neuron(neuron_type type, int id, double Y, double X, double ActResponse)
    :NeuronType(type),ID(id),y(Y),x(X),activationResponse(1),output(0)
{}

void Neuron::showInfo()
{

    std::cout<<"Id:"<<ID<<" NeuronType:";
    switch (NeuronType) {
    case input:
        std::cout<<"input ";
        break;
    case neuron_type::output:
        std::cout<<"output ";
        break;
    case hidden:
        std::cout<<"hidden ";
        break;
    case bias:
        std::cout<<"bias ";
        break;
    case unknown:
        std::cout<<"unknown ";
        break;
    }
    std::cout<<" output:"<<output<<std::endl;
}
