#include "enums.hpp"
#include <stdio.h>
#include <iostream>
#include "gene.h"
#include <string>

class NeuronGene:public Gene{
public:
    int ID;
    neuron_type type;
    bool recurrent; //loop
    double activation;
    double x,y;

    NeuronGene();

    NeuronGene(int neuronID, neuron_type NT, bool rec, double X=0, double Y=0);

    void showInfo();
};
