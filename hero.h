#ifndef HERO_H
#define HERO_H
#include "object.h"
#include <SFML/Graphics.hpp>

class Hero : public object
{
    public:
    double ySpeed;
    double gravity;
    bool inAir;
    sf::RectangleShape rect;
    void update(float time);
    void move(float x, float y);
    Hero();
};

#endif // HERO_H
