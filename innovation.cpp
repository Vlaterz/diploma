#include "innovation.h"


Innovation::Innovation(int OUTneuron, int INneuron, int innovationID)
    :neuronIN(INneuron), neuronOUT(OUTneuron),innovationID(innovationID)
{
    innovationType = new_link;
    typeOfNeuron=unknown;
    neuronID=-1;
}

Innovation::Innovation(int idOfNeuron, neuron_type type, int innovationID)
    :neuronID(idOfNeuron), typeOfNeuron(type), innovationID(innovationID)
{
    innovationType = new_neuron;
    neuronIN=-1;
    neuronOUT=-1;
}

Innovation::Innovation(int out, int in, innov_type InnovType, neuron_type neurType, int innovationID)
    : neuronOUT(out),neuronIN(in),innovationType(InnovType), typeOfNeuron(neurType),innovationID(innovationID)
{
}

Innovations::Innovations():lastInnovationId(0)
{ 
    innovations.resize(0);
}

void Innovations::addInnovation(int fromNeuron, int toNeuron)
{
    innovations.push_back(Innovation(fromNeuron,toNeuron,lastInnovationId++));
}

void Innovations::addInnovation(int neuronID, neuron_type type)
{
    innovations.push_back(Innovation(neuronID, type, lastInnovationId++));
}

int Innovations::getInnovation(int neuronFrom, int neuronTo)
{
    for(unsigned int i=0 ; i<innovations.size() ; i++)
    {
        if(   innovations[i].neuronOUT == neuronFrom &&
                innovations[i].neuronIN == neuronTo)
        {
            return innovations[i].innovationID;
        }

    }
    return -1;
}

int Innovations::getInnovation(int neuronID, neuron_type neuroType)
{
    for(unsigned int i=0 ; i<innovations.size() ; i++)
    {
        if(innovations[i].neuronID == neuronID && innovations[i].typeOfNeuron==neuroType)
        {

            return innovations[i].innovationID;
        }
    }
    return -1;
}

int Innovations::getLastInnovationID()
{
    int maxID=-1;
    for(unsigned int i=0 ; i<innovations.size() ; ++i)
    {
        if(maxID<innovations[i].innovationID )
            maxID=innovations[i].innovationID;
    }
    return maxID;
}

int Innovations::getNeuronId(int innovationID)
{
    for(unsigned int i = 0 ; i<innovations.size() ; i++)
    {
        if(innovations[i].innovationID == innovationID)
        {
            return innovations[i].neuronID;
        }
    }
    return -1;
}

int Innovations::getLastNeuronId()
{
    int maxID=-1;
    for(unsigned int i=0 ; i<innovations.size() ;i++)
    {
        if(innovations[i].innovationType==new_neuron && innovations[i].neuronID > maxID)
        {
            maxID=innovations[i].neuronID;
        }
    }
    return maxID;
}

int Innovations::getLastSynapseID()
{
    int maxID=-1;
    for(unsigned int i=0 ; i<innovations.size() ;i++)
    {
        if(innovations[i].innovationType==new_link && innovations[i].innovationID > maxID)
        {
            maxID=innovations[i].innovationID;
        }
    }
    return maxID;
}

Innovation *Innovations::getInnovationByID(int innovID)
{
    for(unsigned int i = 0 ;i<innovations.size() ; i++)
    {
        if(innovations[i].innovationID == innovID)
        {
            return &innovations[i];
        }
    }
    return NULL;
}

void Innovations::showInfo()
{
    std::cout<<"--------------List of all innovations---------------"<<std::endl;
    for(unsigned int i =0; i<innovations.size() ; i++)
    {
        std::cout<<"ID:"<<innovations[i].innovationID
                <<" Type:"<<innovations[i].innovationType
               <<" OUT:"<<innovations[i].neuronOUT
              <<" IN:"<<innovations[i].neuronIN
             <<" NeuronID:"<<innovations[i].neuronID
            <<" NeuronType:"<<innovations[i].typeOfNeuron
           <<" InnovationType:"<<innovations[i].innovationType
           <<std::endl;
    }
    std::cout<<"--------------END---------------"<<std::endl;
}
