#include "synapseGene.h"



void SynapseGene::showInfo()
{
      std::cout<<"ID:"<<ID
             <<" FROM:"<<linkFrom
            <<" TO:"<<linkTo
           <<" weight:"<<weight
          <<" recurrent:"
         <<recurrent;
      if(enabled)
         std::cout<<std::endl;
      else
          std::cout<<" _DISABLED_"<<std::endl;
}

bool SynapseGene::operator ==(SynapseGene right)
{
    if(this->enabled==right.enabled
            && this->geneType==right.geneType
            && this->linkFrom==right.linkFrom
            && this->linkTo==right.linkTo
            && this->recurrent==right.recurrent)
        return true;
    else
        return false;
}

bool SynapseGene::operator !=(SynapseGene right)
{
    if(*this == right)
    {
        return false;
    }
    else
    {
        return true;
    }
}
