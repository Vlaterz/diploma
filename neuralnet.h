#ifndef NEURALNET_H
#define NEURALNET_H
#include <iostream>
#include "neuron.h"
#include "synapse.h"
#include <vector>
#include "enums.hpp"
#include "math.h"
class NeuralNet
{
std::vector<Neuron*> vecNeurons;
int depth;
public:
    NeuralNet();
    ~NeuralNet(){}
    NeuralNet(std::vector<Neuron*> neurons, int Depth);

    std::vector<double> Update(const std::vector<double> &inputs, const run_type type);

    double Sigmoid(double net,double curvature){return 1/(1+exp(-net/curvature));}
};

#endif // NEURALNET_H
