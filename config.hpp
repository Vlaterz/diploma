#ifndef CONFIG_H
#define CONFIG_H

namespace params{
//chances of mutation
static const double linkMutationChance = 0.7;
static const double neuronMutationChance = 0.6;
static const double reccurentChance = 0;

static const double CrossoverRate = 0.7; //chance of crossOvering 2 Genomes

//weight mutations
static const double weightMutationChance = 0.9;
static const double chanceOfWeightPerturbation = 0.3;
static const double chanceOfWeightChange = 0.1;
static const double weightMaxPerturbation = 0.1;


static const double NeuronActivationMutationProbability = 0.1;
static const double NeuronActivationPerturbation = 0.1;


// bonuses/penalties
static const double YoungBonusAgeThreshhold=5;
static const double OldAgeThreshold=15;
static const double YoungFitnessBonus=2; //bonus for new Genomes
static const double OldFitnessPenalty=0.5; //penalty for high fitnesses of Genomes

static unsigned const int maxPermittedNeurons = 10;
static const int MaxGenomesInSpecies=10;
static const int PopulationSize = 10; //how many genomes can specie spawn

//coeffs for calculation of delta of 2 genomes
static const double excessGeneCoefficient = 2;
static const double disjointedGeneCoefficient = 2;
static const double weightCoefficient = 1;
static const double similarDeltaCoefficient = 6;

static const double bestToSurvivePercent = 25;
static unsigned const int noImprovementTime = 10;

static unsigned const int numBestGenomes = 3;
}

#endif // CONFIG_H
