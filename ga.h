﻿#ifndef GA_HPP
#define GA_HPP
#include "genome.h"
#include "config.hpp"
#include <math.h>
#include "species.h"

double testFitnessFunction(double input , double answer);
double testXorFunction(std::vector<int> inputs , double answer);
double testANDFunction(std::vector<int> inputs , double answer);
class Ga{

    int generation;
    int nextGenomeId;
    int nextSpeciesId;
    std::vector<Genome> bestGenomes;
    double bestEverFitness;
    std::vector<Genome> Genomes;
    Innovations innovations;
    std::vector<Species> species;

    public:

    void createNewPopulation();
    std::vector<Genome> createFirstPopulation(int numberOfSpecies, int inputs, int outputs , Innovations& I);
    std::vector<Genome> createFirstPopulationNoLinks(int numberOfSpecies, int inputs, int outputs , Innovations& I);

    std::vector<NeuralNet> Epoch2(const std::vector<double>& fitnessScores);
    void work(unsigned int numGenerations);


    Ga():generation(0),nextGenomeId(0),nextSpeciesId(0),bestEverFitness(0){}

    void speciesPurge();
    void updateScores(const std::vector<double>& fitnessScores);
    void updateBestGenomes();
    void SpeciateGenomes();
    void UpdateSpeciesFitness();
    void CalculateSpeciesSpawnAmounts();//diff
    std::vector<NeuralNet> CreateNeuralNetworks();

    Genome MakeCrossoverBaby(Species& s, int next_id, Genome& prev_baby);
    Genome crossover(Genome& mom, Genome& dad, Innovations& I);
    Genome crossover2(Genome& mom, Genome& dad, Innovations& I);

    void sortGenomes();
    void sortSpecies();

    void showInfo();
    void showUI();

    Genome& tournamentSelection(int N);
    double deltaOfSpecies(Genome& one, Genome& two);

    Innovations* GetInnovations(){return &innovations;}
    Genome *getGenome(int i);
    std::vector<Genome>* getGenomes(){return &Genomes;}

};

#endif // GA_HPP
