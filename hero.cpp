#include "hero.h"

void Hero::update(float time)
{
    rect.move(sf::Vector2f(0,time*ySpeed));
    ySpeed -=gravity;
    if(rect.getPosition().y > 400)
    {
        ySpeed = 0;
        inAir=false;
        //rect.move(0,-1);
    }
    else
    {
        inAir=true;
    }
}

void Hero::move(float x, float y)
{
    rect.move(x,y);
    //rect.setPosition(sf::Vector2f(0,0));
}

Hero::Hero()
{
    inAir=false;
    rect.setFillColor(sf::Color::White);
    rect.setSize(sf::Vector2f(30,100));
    rect.setPosition(sf::Vector2f(0,400));
    rect.setPosition(50,250);
    gravity = -0.1;
}
