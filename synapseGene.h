#include <iostream>
#include <stdio.h>
#include "gene.h"
class SynapseGene:public Gene{
public:
    int linkFrom;
    int linkTo;
    double weight;
    int ID;
    bool enabled;
    bool recurrent;

    SynapseGene():ID(-1){}
    SynapseGene(int from, int to, double weight, int ID, bool recurrent, bool working)
        :linkFrom(from),linkTo(to),weight(weight),ID(ID),enabled(working),recurrent(recurrent)
        { geneType = synapse;}

    void showInfo();

    bool operator ==(SynapseGene right);
    bool operator !=(SynapseGene right);

};
