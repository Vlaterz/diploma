#ifndef SPECIES_H
#define SPECIES_H
#include "genome.h"
#include <vector>
#include "config.hpp"
class Species
{
    //std::vector<Genome*> vecMembers;
    int ID;
    unsigned int noImprovementTime;
    unsigned int age;
    double bestFitness;
    double spwnReq;
    Genome leader;
    std::vector<Genome*> vecMembers;

    public:

    Species();
    Species(Genome &First, int speciesID);
    void addMember(Genome& newGenome);
    void purge();
    //adjusts fitnesses and counts something
    void adjustFitnesses();
    //calculates how many offspring this species should spawn
    void CalculateSpawnAmount();
    void Sort();
    void showInfo();
    Genome Spawn();
//------------------------accessor methods---------------------
    Genome*  Leader(){return &leader;}
    double   NumToSpawn()const{return spwnReq;}
    int      NumMembers()const{return vecMembers.size();}
    int      timeWithNoImprove()const{return noImprovementTime;}
    int      Id()const{return ID;}
    double   SpeciesLeaderFitness(){return leader.Fitness();}
    double   BestFitness()const{return bestFitness;}
    int      Age()const{return age;}
    int      NoImprovementTime()const{return noImprovementTime;}
    int      Size()const{return vecMembers.size();}

    void SetNoImprovementTime(int time){noImprovementTime=time;}

    friend bool operator<(const Species &lhs, const Species &rhs)
    {
      return lhs.bestFitness > rhs.bestFitness;
    }
};


#endif // SPECIES_H
