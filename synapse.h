//#include "includes.hpp"
#ifndef SYNAPSE_H
#define SYNAPSE_H
class Neuron;
#include "neuron.h"

class Synapse
{
public:
    double weight;
    Neuron* In;
    Neuron* Out;
    bool rec;

    Synapse(){}
    Synapse(double w,Neuron* in,Neuron* out, bool r):weight(w),In(in),Out(out),rec(r){}
};

#endif // SYNAPSE_H
