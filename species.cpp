#include "species.h"

Species::Species(){}

Species::Species(Genome &First, int speciesID)
    :ID(speciesID),leader(First)
{
 vecMembers.push_back(&First);
 leader = First;
 bestFitness=0;
 noImprovementTime=0;
 spwnReq = 0;
 age=0;
}

void Species::adjustFitnesses()
{
 //std::cout<<"adjustFitnesses START"<<std::endl;
 double total=0;//total adj fitness
 double sumFitness=0;

 //GIVE BONUS TO NEW GENOMES AND PUNISH OLD ONES and calculate adjusted fitnesses
 for(unsigned int i=0 ; i<vecMembers.size() ; i++)
 {
    sumFitness+=vecMembers[i]->Fitness();
    double fitness = vecMembers[i]->Fitness();
    if(age<params::YoungBonusAgeThreshhold)
    {
        fitness*=params::YoungFitnessBonus;
    }
    if(age>params::OldAgeThreshold)
    {
        fitness*=params::OldFitnessPenalty;
    }
    total += fitness;
    double adjustedFitness = fitness/vecMembers.size();
    vecMembers[i]->setAdjFitness(adjustedFitness);
 }

 for(unsigned int i=0 ; i<vecMembers.size() ; i++)
 {
     if(vecMembers[i]->Fitness() > bestFitness)
     {
         bestFitness=vecMembers[i]->Fitness();
         noImprovementTime=0;
     }
 }

// std::cout<<"adjustFitnesses END"<<std::endl;

}

void Species::addMember(Genome &newGenome)
{
    vecMembers.push_back(&newGenome);
}


void Species::purge()
{
    vecMembers.clear();
    ++age;
    //++noImprovementTime;
    bool becameBetter = false;
    for(auto& g : vecMembers)
    {
        if(g->Fitness() > bestFitness)
        {
            becameBetter = true;
            noImprovementTime=0;
        }

    }
    if(!becameBetter)
        ++noImprovementTime;
}

void Species::CalculateSpawnAmount()
{
    //std::cout<<"Species::CalculateSpawnAmount START"<<std::endl;
    spwnReq=0;
    double averageAdjFitness;
    for(auto& g : vecMembers)
    {
        averageAdjFitness+=g->AdjustedFitness();
    }
    averageAdjFitness /= vecMembers.size();

    bool zero = false;//new pure specie
    if(bestFitness == 0)
    {
        zero = true;
    }
    for(unsigned int i=0 ; i<vecMembers.size() ; i++)
    {
        if(zero)
            vecMembers[i]->SetSpawnAmount(1);
        else
            vecMembers[i]->SetSpawnAmount(vecMembers[i]->AdjustedFitness() / averageAdjFitness);
        spwnReq+=vecMembers[i]->AmountToSpawn();
    }
    //std::cout<<"Species::CalculateSpawnAmount END"<<std::endl;
}

void Species::Sort()
{
    for(unsigned int i=0 ; i<vecMembers.size() ;i++)
    {
        for(unsigned int j=i ; j<vecMembers.size() ; j++)
        {
            if(vecMembers[i] < vecMembers[j])
            {
                Genome temp = *vecMembers[i];
                *vecMembers[i] = *vecMembers[j];
                *vecMembers[j] = temp;
            }
        }
    }
}

Genome Species::Spawn()
{
    double winnersCount=vecMembers.size()+1;
    winnersCount /= 100;
    winnersCount *=params::bestToSurvivePercent;
    return *vecMembers[RandINT(0,winnersCount)];
}

void Species::showInfo()
{
    std::cout<<"ID:"<<ID<<" AGE:"<<age<<std::endl<<
        /* <<"averageFitness:"<<averageFitness<<" averageAdjFitness:"<<averageAdjFitness<<*/" bestFitness:"<<bestFitness<<std::endl
        <<" NumMembers:"<<NumMembers()+1<<" spwnReq:"<<spwnReq<<" noImprTime:"<<noImprovementTime<<std::endl
       <<"leaderID:  "<<leader.Id()<<" FITNESS:"<<leader.Fitness()<<" adjFitness: "<<leader.AdjustedFitness()<<" amountToSpwn: "
      <<leader.AmountToSpawn()<<std::endl;
    for(unsigned int i=0 ; i<vecMembers.size() ; i++)
    {
        std::cout<<"ID:"<<vecMembers[i]->Id()<<" fitness: "<<vecMembers[i]->Fitness()<<" adjFitness: "<<vecMembers[i]->AdjustedFitness()<<" amountToSpwn: "
                <<vecMembers[i]->AmountToSpawn()<<std::endl;
    }
}
