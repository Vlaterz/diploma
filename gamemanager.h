#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include <SFML/Graphics.hpp>
#include <cstdlib>
#include "hero.h"
#include "wall.h"
#include "genome.h"
#include <SFML/Graphics/Text.hpp>
#include "ga.h"
class GameManager
{
public:
    std::vector<wall> walls;
    Hero hero;
    double speed;
    double score;

    std::vector<double> inputsForNN;
    std::vector<double> scores;
    Ga* algo;
    int currentGenome;
    std::vector<Genome> * genomes;
    bool epochTime;

    GameManager();
    void update(double time);
    void collisionCheck();

};

#endif // GAMEMANAGER_H
